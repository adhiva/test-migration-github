const express = require("express");
const { testController, packageCategoryController } = require("../controllers");
const router = express.Router();
/**
 * @api {get} /test/ Request Test information
 * @apiName GetTest
 * @apiGroup Test
 *
 * @apiSuccess {String} name Name of the User.
 */
router.get("/test", testController.get);
/**
 * @api {post} /test/:id Request Test information
 * @apiName CreateTest
 * @apiGroup Test
 *
 * @apiParam {Number} id User's unique ID.
 *
 * @apiSuccess {String} name Name of the User.
 */
router.get("/test/:id", testController.getDetail);
router.put("/test/:id", testController.update);
router.post("/test", testController.create);


// /**
//  * @api {post} /package_category Create a new package_category
//  * @apiVersion 0.3.0
//  * @apiName post package_category
//  * @apiGroup package_category
//  * @apiPermission none
//  *
//  * @apiDescription In this case "apiErrorStructure" is defined and used.
//  * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
//  *
//  * @apiParam {String} name Name of the package_category.
//  * @apiParam {String} description Name of the package_category.
//  *
//  * @apiSuccess {String} name Name of the package_category.
//  * @apiSuccess {String} description Name of the package_category.
//  *
//  * @apiUse PackageCategoryError
//  */

router.get("/package_category", packageCategoryController.get);
router.post("/package_category", packageCategoryController.create);
router.get("/package_category/:id", packageCategoryController.getDetail);
router.put("/package_category/:id", packageCategoryController.update);
router.delete("/package_category/:id", packageCategoryController.delete);


module.exports = router;
