const TestModel = require("../models").Test;
const { validateRequest, sendResponseSuccess } = require("../utils");

module.exports = {
  async create(req, res, next) {
    const schema = {
      type: "object",
      required: ["name"],
      properties: {
        name: { type: "string" },
      },
    };
    validateRequest(req.body, schema);

    try {
      await TestModel.create({
        name: req.body.name,
      });
      sendResponseSuccess(res, null, 201);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async get(req, res, next) {
    try {
      const data = await TestModel.findAll();
      sendResponseSuccess(res, data, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async getDetail(req, res, next) {
    try {
      const { id } = req.params;
      const data = await TestModel.findByPk(id);
      sendResponseSuccess(res, data, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async update(req, res, next) {
    try {
      const { id } = req.params;
      await TestModel.update({ name: req.body.name }, { where: { id } });
      sendResponseSuccess(res, null, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
};
