const package_category = require("../../models").package_category;
const { validateRequest, sendResponseSuccess } = require("../../utils");

module.exports = {
  async create(req, res, next) {
    const schema = {
      type: "object",
      required: ["name", "description"],
      properties: {
        name: { type: "string" },
        description: { type: "string" },
      },
    };
    validateRequest(req.body, schema, res);
    const { name, description } = req.body;

    try {
      await package_category.create({ name, description });
      sendResponseSuccess(res, null, 201);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async get(req, res, next) {
    try {
      const data = await package_category.findAll();
      sendResponseSuccess(res, data, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async getDetail(req, res, next) {
    try {
      const { id } = req.params;
      const data = await package_category.findByPk(id);
      sendResponseSuccess(res, data, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async update(req, res, next) {
    try {
      const { id } = req.params;
      const { name, description, updated_by } = req.body;
      const schema = {
        type: "object",
        required: ["name", "description", "updated_by"],
        properties: {
          name: { type: "string" },
          description: { type: "string" },
          updated_by: { type: "string" },
        },
      };
      validateRequest(req.body, schema, res);

      await package_category.update(
        { name, description, updated_by },
        { where: { id } }
      );
      sendResponseSuccess(res, null, 200);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
  async delete(req, res, next) {
    try {
      const { id } = req.params;
      await package_category.destroy({ where: { id } });
      sendResponseSuccess(res, null, 204);
    } catch (e) {
      next(e);
      throw new Error(e.message);
    }
  },
};
