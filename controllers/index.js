const testController = require("./test.controller");
const packageCategoryController = require("./package_category/package_category.controller");

module.exports = {
  testController,
  packageCategoryController,
};
