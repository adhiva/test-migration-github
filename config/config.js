var dotenv = require("dotenv").config();

module.exports = {
  development: {
    username: dotenv.parsed.MYSQL_DB_USERNAME,
    password: dotenv.parsed.MYSQL_DB_PASSWORD,
    database: dotenv.parsed.MYSQL_DB_DATABASE,
    host: dotenv.parsed.MYSQL_DB_HOST,
    dialect: "mysql",
    logging: false,
  },
  test: {
    username: dotenv.parsed.MYSQL_DB_USERNAME,
    password: dotenv.parsed.MYSQL_DB_PASSWORD,
    database: dotenv.parsed.MYSQL_DB_DATABASE,
    host: dotenv.parsed.MYSQL_DB_HOST,
    dialect: "mysql",
    logging: false,
  },
  production: {
    username: dotenv.parsed.MYSQL_DB_USERNAME,
    password: dotenv.parsed.MYSQL_DB_PASSWORD,
    database: dotenv.parsed.MYSQL_DB_DATABASE,
    host: dotenv.parsed.MYSQL_DB_HOST,
    dialect: "mysql",
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
