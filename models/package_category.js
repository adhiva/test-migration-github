"use strict";
module.exports = (sequelize, DataTypes) => {
  const package_category = sequelize.define(
    "package_category",
    {
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      deleted_at: DataTypes.DATE,
      deleted_by: DataTypes.STRING,
      updated_by: DataTypes.STRING,
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at",
      },

      updatedAt: {
        type: DataTypes.DATE,
        field: "updated_at",
      },
    },
    {
      tableName: "package_category",
      createdAt: "created_at",
      updatedAt: "updated_at",
      deletedAt: "deteled_at",
      timestamps: true,
      underscored: true,
      freezeTableName: true,
      paranoid: true,
    }
  );
  package_category.associate = function (models) {
    // associations can be defined here
  };
  return package_category;
};
