class AppError extends Error{
    constructor(message, status){
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 500;
    }
}

class NotFoundError extends AppError{
    constructor(message, status){
        super(message || 'Data not found!');
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 404;
    }
}

class ForbiddenError extends AppError{
    constructor(message, status){
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 403;
    }
}

class UniqueError extends AppError{
    constructor(message, status){
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 409;
    }
}

class RequestValidationError extends AppError{
    constructor(message, status, reason){
        super(message|| 'Bad request');
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 400;
        this.reason = reason || 'unknown'
    }
}

module.exports = {
    NotFoundError,
    ForbiddenError,
    UniqueError,
    RequestValidationError,
    AppError,
};
