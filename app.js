const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./routes");
const { errorHandler,logger } = require("./utils/index");
const models = require("./models");
const app = express();
const port = process.env.APP_PORT || 3000;
const prefix = "aqiqah/v1";

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

logger();

models.sequelize
  .sync()
  .then(() => console.log("\x1b[35m", "Database connected !"))
  .catch((err) => console.error(err, "something went wrong with the DB !"));

app.get('/', (req, res) => res.send("App is working"));

app.use(`/docs`, express.static("public"));
app.use("/aqiqah/v1", routes);
app.use((err, req, res, next) => errorHandler(err, res));

app.listen(port, () => {
  console.log(
    "\x1b[34m",
    `The Server is running at ${port} in ${process.env.APP_NAME} mode`
  );
});

module.exports = { app };
