const webpack = require("webpack");
const Apidoc = require("webpack-apidoc");
const path = require("path");

const SRC_DIR = path.resolve(__dirname, "");
const OUTPUT_DIR = path.resolve(__dirname, "dist");

module.exports = {
  entry: `${SRC_DIR}/app.js`,
  output: {
    path: `${OUTPUT_DIR}`,
    publicPath: "/",
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: [{ loader: "babel-loader" }],
      },
    ],
  },
  plugins: [
    new Apidoc({
      src: "./routes",
      dest: "./public",
      includeFilters: [".*\\.js$"],
      excludeFilters: ["node_modules/"],
    }),
  ],
  target: "web",
  mode: "development",
};
