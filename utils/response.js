const Ajv = require("ajv");
const ajv = new Ajv();
const { errorHandler } = require("./middleware");

const validateRequest = (request, schema, res) => {
  let valid = ajv.validate(schema, request);
  if (!valid) {
    throw errorHandler(ajv.errors, res);
  }
};

const sendResponseSuccess = (res, data, statusCode) => {
  if (data != null)
    return res.status(statusCode).json({
      status: "success",
      statusCode: statusCode || 200,
      message: "Success!",
      data: data,
    });

  return res.status(statusCode).json({
    status: "success",
    statusCode: statusCode || 200,
    message: "Success!",
  });
};

module.exports = {
  validateRequest,
  sendResponseSuccess,
};
