const {
  RequestValidationError,
  NotFoundError,
  ForbiddenError,
} = require("../exceptions/business_exceptions");

const errorHandler = (err, res) => {
  const responseObj = {
    status: "error",
    statusCode: err.status,
    message: err.message,
  };
  console.error(err);
  log.error(err, responseObj);
  if (err instanceof RequestValidationError) {
    return res.status(err.status).json({ ...responseObj, reason: err.reason });
  } else if (err instanceof NotFoundError) {
    return res.status(e.status).json(responseObj);
  } else if (err instanceof ForbiddenError) {
    return res.status(e.status).json(responseObj);
  } else {
    return res.status(500).json({
      ...responseObj,
      message: "Internal server error!",
      reason: err,
    });
  }
};

module.exports = {
  errorHandler,
};
