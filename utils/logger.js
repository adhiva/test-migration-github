const simpleLogger = require("simple-node-logger");
const dayjs = require("dayjs");

const options = {
  logFilePath: `./logs/log-${dayjs().format("DD-MM-YYYY")}.log`,
  timestampFormat: "YYYY-MM-DD HH:mm:ss.SSS",
};
const logger = () => global.log = simpleLogger.createSimpleLogger(options);

module.exports = logger;
